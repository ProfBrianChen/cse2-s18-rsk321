// Ryan Kessler
// CSE2
// 2-20-18
// hw04
import java.util.Scanner;
public class Yahtzee{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner input = new Scanner( System.in );
    System.out.print("Would you like to enter a dice roll? (answer with y/n): "); // ask the user if they want to input a roll or randomly generate it
    char answer = input.next().charAt(0);
    int error = 0; // defaulting the error statement to false
    int dice1, dice2, dice3, dice4, dice5; // declaration of the five dice
    dice1 = dice2 = dice3 = dice4 = dice5 = 0; // initialization
    
    // below are the if statements for the 5 dice, if the user inputs an invalid dice value it will change the error statement to true and end the code
    if ( answer == 'y' ) {
      System.out.print("Please enter your first dice roll: ");
      dice1 = input.nextInt();
      if ( dice1 == 0 || dice1 > 6 ) {
        error = 1;
      }
      else {
        System.out.print("Please enter your second dice roll: ");
        dice2 = input.nextInt();
        if ( dice2 == 0 || dice2 > 6 ) {
          error = 1;
        }
        else {
          System.out.print("Please enter your third dice roll: ");
          dice3 = input.nextInt();
          if ( dice3 == 0 || dice3 > 6 ) {
            error = 1;
          }
          else {
            System.out.print("Please enter your fourth dice roll: ");
            dice4 = input.nextInt();
            if ( dice4 == 0 || dice4 > 6 ) {
            error = 1;
            }
            else {
              System.out.print("Please enter your fifth dice roll: ");
              dice5 = input.nextInt();
              if ( dice5 == 0 || dice5 > 6 ) {
                error = 1;
              }
              else {
                String diceRoll = "" + dice1 + dice2 + dice3 + dice4 + dice5;
              }
            }
          }
        }
      }
    }
    // below is the randomization of the 5 rolls if the user requests random rolls
    else if ( answer == 'n' ) {
      dice1 = (int)(Math.random()*6)+1;
      dice2 = (int)(Math.random()*6)+1;
      dice3 = (int)(Math.random()*6)+1;
      dice4 = (int)(Math.random()*6)+1;
      dice5 = (int)(Math.random()*6)+1;
      String diceRoll = "" + dice1 + dice2 + dice3 + dice4 + dice5;
      System.out.println("Your dice roll is " + diceRoll);
    }
    // error statement for if the user inputs an invalid response
    else {
      System.out.println("Error: please input y for yes or n for no");
      error = 2;
    }
    int numAce = 0, numTwo = 0, numThree = 0, numFour = 0, numFive = 0, numSix = 0; // declaration and initialization of the quantitty of specific rolls
    
    // for each case below, the code will determine what value is rolled, and then increment the number quantity
    switch(dice1) {
      case 1:
        numAce += 1; 
        break;
      case 2:
        numTwo += 1;
        break;
      case 3:
        numThree += 1;
        break;
      case 4:
        numFour += 1;
        break;
      case 5:
        numFive +=1;
        break;
      case 6:
        numSix += 1;
        break;
    }
    switch(dice2) {
      case 1:
        numAce += 1;
        break;
      case 2:
        numTwo += 1;
        break;
      case 3:
        numThree += 1;
        break;
      case 4:
        numFour += 1;
        break;
      case 5:
        numFive +=1;
        break;
      case 6:
        numSix += 1;
        break;
    }
    switch(dice3) {
      case 1:
        numAce += 1;
        break;
      case 2:
        numTwo += 1;
        break;
      case 3:
        numThree += 1;
        break;
      case 4:
        numFour += 1;
        break;
      case 5:
        numFive +=1;
        break;
      case 6:
        numSix += 1;
        break;
    }
    switch(dice4) {
      case 1:
        numAce += 1;
        break;
      case 2:
        numTwo += 1;
        break;
      case 3:
        numThree += 1;
        break;
      case 4:
        numFour += 1;
        break;
      case 5:
        numFive +=1;
        break;
      case 6:
        numSix += 1;
        break;
    }
    switch(dice5) {
      case 1:
        numAce += 1;
        break;
      case 2:
        numTwo += 1;
        break;
      case 3:
        numThree += 1;
        break;
      case 4:
        numFour += 1;
        break;
      case 5:
        numFive +=1;
        break;
      case 6:
        numSix += 1;
        break;
    }
    
    int scoreAce, scoreTwo, scoreThree, scoreFour, scoreFive, scoreSix; // declaration of the upper score values
    int score3ofK, score4ofK, scoreFull, scoreSmStraight, scoreLgStraight, scoreYahtzee, scoreChance; // declaration of the lower score values
    scoreAce = scoreTwo = scoreThree = scoreFour = scoreFive = scoreSix
      = score3ofK = score4ofK = scoreFull = scoreSmStraight = scoreLgStraight // initialization of the score values to zero
      = scoreYahtzee = scoreChance = 0;
    char chance = 'n';
    
    // below is the switch statement to decide if the scoring should run, based on the error boolean (true/false statement)
    switch (error) {
      case 1:
        System.out.println("You entered an impossible dice roll, please try again");        // telling the user they inputed an invalid response
        break;
      case 0: // running the scoring
        if ( dice1 == dice2 && dice2 == dice3 && dice3 == dice4 && dice4 == dice5 ) { 
          scoreYahtzee = 50;
          System.out.println("Yahtzee!!!");
        }
        else if ( numAce >= 1 && numTwo >= 1 && numThree >= 1 && numFour >= 1 && numFive >= 1       // determining if the user rolled a large straight
                || numSix >= 1 && numTwo >= 1 && numThree >= 1 && numFour >= 1 && numFive >= 1) {
          scoreLgStraight = 40;
          System.out.println("Large Straight");
        }
        else if ( numAce >= 1 && numTwo >= 1 && numThree >= 1 && numFour >= 1 ||     // determining if the user rolled a small straight
                numTwo >= 1 && numThree >= 1 && numFour >= 1 && numFive >= 1 ||
                numThree >= 1 && numFour >= 1 && numFive >= 1 && numSix >= 1 ) {
          scoreSmStraight = 30;
          System.out.println("Small Straight");
        }
        else if ( numAce == 3 && (numTwo == 2 || numThree == 2 || numFour == 2 || numFive == 2 || numSix == 2) ||    // determining if the user rolled a full house
                numTwo == 3 && (numAce == 2 || numThree == 2 || numFour == 2 || numFive == 2 || numSix == 2) ||
                numThree == 3 && (numAce == 2 || numTwo == 2 || numFour == 2 || numFive == 2 || numSix == 2) ||
                numFour == 3 && (numAce == 2 || numTwo == 2 || numThree == 2 || numFive == 2 || numSix == 2) ||
                numFive == 3 && (numAce == 2 || numTwo == 2 || numThree == 2 || numFour == 2 || numSix == 2) ||
                numSix == 3 && (numAce == 2 || numTwo == 2 || numThree == 2 || numFour == 2 || numFive == 2) ) {
          scoreFull = 25;
          System.out.println("Full House");
        }
        else if ( numAce == 4 || numTwo == 4 || numThree == 4 || numFour == 4 || numFive == 4 || numSix == 4 ) {    // determining if the user rolled four of a kind
          score4ofK = dice1 + dice2 + dice3 + dice4 + dice5;
          System.out.println("Four of a Kind");
        }
        else if ( numAce == 3 || numTwo == 3 || numThree == 3 || numFour == 3 || numFive == 3 || numSix == 3 ) {    // determining if the user rolled three of a kind
          score3ofK = dice1 + dice2 + dice3 + dice4 + dice5;
          System.out.println("Three of a Kind");
        }
        else {
          System.out.print("Have you used 'chance' yet? (y/n): ");  // with only one roll, the upper portion of the scorecard would never be used, to make it relevant,
          chance = input.next().charAt(0);                          //  I decided to ask the user if they had previously used chance
          if ( chance == 'n') {   // if the user has not used chance, the code uses chance
            scoreChance = dice1 + dice2 + dice3 + dice4 + dice5;
            System.out.println("Chance");
          }
          else if ( chance == 'y' ) {            // if the user has used chance, the code below determines which portion of the upper scorecard to use
            if ( numSix >= 2 && numSix >= numFive ) {
              scoreSix = numSix * 6;
            }
            else if (numFive >= 2 && numFive >= numFour ) {
              scoreFive = numFive * 5;
            }
            else if (numFour >= 2 && numFour >= numThree ) {
              scoreFour = numFour * 4;
            }
            else if (numThree >= 2 && numThree >= numTwo ) {
              scoreThree = numThree * 3;
            }
            else if (numTwo >= 2 && numTwo >= numAce ) {
              scoreTwo = numTwo * 2;
            }
            else {
              scoreAce = numAce;
            }
          }
          else {
            System.out.println("Failed to answer chance question"); // this will be printed if the user does not properly respond to the chance question
          }
        }
        break;
    }
    int scoreTotal = scoreAce + scoreTwo + scoreThree + scoreFour + scoreFive + scoreSix // declaring and initializing the total score
      + score3ofK + score4ofK + scoreFull + scoreSmStraight + scoreLgStraight
      + scoreYahtzee + scoreChance;
    System.out.println("Your final score is: " + scoreTotal);
    System.out.println(numAce + " " + numTwo + " " + numThree + " " + numFour + " " + numFive + " " + numSix); // printing the total score
  }  //end of main method   
} //end of class