// Ryan Kessler
// CSE2
// 2-2-18
// Lab02 - program will output information about cycling trips based on set inputs
public class Cyclometer {
  // main method required for every Java program
  public static void main(String[] args) {
    int secsTrip1=480; // the amount of time trip one took
    int secsTrip2=3220; // the amount of time trip two took
    int countsTrip1=1561; // number of wheel rotations that occured in trip one
    int countsTrip2=9037; // number of wheel rotations that occured in trip two
    double wheelDiameter=27.0,  // diameter of the wheel, used to calculate distance traveled
  	PI=3.14159, // value of pi needed to calculate distance traveled
  	feetPerMile=5280,  // conversion factor from feet to miles
  	inchesPerFoot=12,   // conversion factor from inches to feet
  	secondsPerMinute=60;  // conversion factor from seconds to minutes
    double distanceTrip1, distanceTrip2,totalDistance;  // defining the three variables the program will solve for
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    distanceTrip1=countsTrip1*wheelDiameter*PI; // gives distance in inches
    	// for each count, a rotation of the wheel travels the diameter in inches times PI
    distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; // dividing by the conversion factors instead gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2; // add the distances to yield the total distance traveled
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  } // end of main method
} // end of class