import java.util.Scanner;
public class lab08 {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int numStudents = (int) (Math.random()*6+5);
    String[] students = new String[numStudents];
    int[] midterm = new int[numStudents];
    System.out.println("Enter " + numStudents + " students names:");
    for(int i=0; i<numStudents; i++) {
      students[i] = input.nextLine();
      midterm[i] = (int) (Math.random()*101);
    }
    System.out.println();
    System.out.println("Here are the midterm grades of the " + numStudents + " students above:");
    for(int i=0; i<numStudents; i++) {
      System.out.println(students[i] + " : " + midterm[i]);
    }
  }
}