// Ryan Kessler
// CSE2
// 3-2-18
// Lab05
import java.util.Scanner; // import scanner
public class TwistGenerator{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner input = new Scanner( System.in ); // create the input scanner
    System.out.print("Enter length as a positive integer: "); // ask the user to enter the length of the twist
    String junkWord = ""; // declare and initialize the junk word variable
    while (input.hasNextInt() == false) { // infinite loop to make sure the user entered the correct for of the varaible
      System.out.print("Enter length as a positive integer: ");
      junkWord = input.next();
    }
    int length = input.nextInt(); // declare and initialize the length of the twist
    int i = 1; // set the counting variable
    int remainder = length % 3; // variable to check what needs to be written at the end of each line
    int repeat = length / 3; // how many times the full twist runs
    // below are three instances of the same loop and switch statements for each of the three lines of the twist
    // the loop prints the amount of full twists, and the switches print the final, partial piece
    while ( i <= repeat ){ // loop 1
      System.out.print("\\ /");
      i++;
    }
    switch (remainder) { // switch 1
      case 1:
        System.out.println("\\");
        break;
      case 2:
        System.out.println("\\ ");
        break;
      default:
        System.out.println("");
        break;
    }
    i = 1; // reset the counting variable
    while ( i <= repeat ){ // loop 2
      System.out.print(" X ");
      i++;
    }
    switch (remainder) { // switch 2
      case 1:
        System.out.println(" ");
        break;
      case 2:
        System.out.println(" X");
        break;
      default:
        System.out.println("");
        break;
    }
    i = 1; // reset the counting variable
    while ( i <= repeat ){ // loop 3
      System.out.print("/ \\");
      i++;
    }
    switch (remainder) { // switch 3
      case 1:
        System.out.println("/");
        break;
      case 2:
        System.out.println("/ ");
        break;
      default:
        System.out.println("");
        break;
    }
  }
}
    