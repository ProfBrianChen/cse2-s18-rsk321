// Ryan Kessler
// CSE2
// 3-2-18
// HW 05
import java.util.Scanner;
public class Hw05{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner input = new Scanner( System.in ); // import the scanner "input"
    String junkWord = ""; // declare and initialize the junk word variable
    // below are the statements to acquire the user's course number
    System.out.print("Enter your course number: ");
    while (input.hasNextInt() == false) {
      System.out.print("Error - please enter your course number as an integer: ");
      junkWord = input.nextLine();
    }
    int courseNumber = input.nextInt();
    
    // below are the statements to aquire the course's department
    junkWord = input.nextLine();
    System.out.print("Enter the course's department: ");
    while (input.hasNextDouble() == true) {
      System.out.print("Error - please enter the department name: ");
      junkWord = input.nextLine();
    }
    String courseDept = input.nextLine();
    
    // below are the statements to aquire the number of times the course meets in a week
    System.out.print("Enter the number of times the course meets in a week: ");
    while (input.hasNextInt() == false) {
      System.out.print("Error - please enter the amount of weekly classes as an integer: ");
      junkWord = input.nextLine();
    }
    int courseWeeklyMeet = input.nextInt();
    
    // below are the statements to aquire the instructor's name
    junkWord = input.nextLine();
    System.out.print("Enter the instructor's name: ");
    while (input.hasNextDouble() == true) {
      System.out.print("Error - please enter the instructor's name: ");
      junkWord = input.nextLine();
    }
    String courseInstructor = input.nextLine();
    
    // below are the statements to aquire the number of times the number of students in the class
    System.out.print("Enter the number of students in the class: ");
    while (input.hasNextInt() == false) {
      System.out.print("Error - please enter the number of students as an integer: ");
      junkWord = input.nextLine();
    }
    int courseStudents = input.nextInt();
    
    // below are the statements to aquire the course's start time
    System.out.print("Enter the hour the course starts: ");
    while (input.hasNextInt() == false) {
      System.out.print("Error - please enter the hour the course starts: ");
      junkWord = input.nextLine();
      junkWord = input.nextLine();
    }
    int courseHour = input.nextInt();
    while ( courseHour < 0 || courseHour > 23 ) {
      System.out.print("Error - please use the 24 hour system: ");
      courseHour = input.nextInt();
    }
    System.out.print("Enter the minute the course starts: ");
    while (input.hasNextInt() == false) {
      System.out.print("Error - please enter the minute the course starts: ");
      junkWord = input.nextLine();
      junkWord = input.nextLine();
    }
    int courseMin = input.nextInt();
    while ( courseMin < 0 || courseMin > 59 ) {
      System.out.print("Error - please enter a valid time in minutes: ");
      courseMin = input.nextInt();
    }
    
    System.out.printf(courseNumber + " - " + courseDept + " - " + courseWeeklyMeet + " - " + courseInstructor + " - " + courseStudents 
                      + " - %02d:%02d %n",courseHour,courseMin);
  }
}