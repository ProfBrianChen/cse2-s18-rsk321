// Ryan Kessler
// 4-17-18
// CSE 2
// HomeWork 09
import java.util.Random;
public class DrawPoker {
  public static void main(String[] args) {
    int[] deck = new int[52]; // creating the array for the deck of cards
    for (int i=0; i<deck.length; i++) { // for loop to fill the deck
      deck[i] = i;
    }
    shuffle(deck);
    int numPlayers = 2;
    int[] player1 = new int[5];
    int[] player2 = new int[5];
    for (int i=0; i<5; i++) {
      player1[i] = deck[i*numPlayers];
      player2[i] = deck[i*numPlayers+1];
    }
    print(player1);
    print(player2);
		System.out.print("Player1: ");
    int priority1 = handCheck(player1);
		System.out.print("Player2: ");
    int priority2 = handCheck(player2);
    if (priority1==priority2) {
      if (high(player1) > high(player2)) {
        System.out.println("Player One Wins!!");
      }
      else if (high(player2) > high(player1)) {
        System.out.println("Player Two Wins!!");
      }
      else {
        System.out.println("It's a Tie!!");
      }
    }
    else if (priority1>priority2) {
      System.out.println("Player One Wins!!");
    }
    else {
      System.out.println("Player Two Wins!!");
    }
  }
  public static boolean flush(int[] hand) {
    if ((int) hand[0]/13 == (int) hand[1]/13 && (int) hand[1]/13 == (int) hand[2]/13 && (int) hand[2]/13 == (int) hand[3]/13 && (int) hand[3]/13 == (int) hand[4]/13) {
      return true;
    }
    else {
      return false;
    }
  }
  public static boolean full(int[] hand) {
    return false;
	}
  public static boolean threeOfK(int[] hand) {
    for (int i=0; i<5; i++) {
      for (int j=0; j<i; j++) {
        for (int k=0; k<j; k++) {
          if (hand[i]%13 == hand[j]%13 && hand[j]%13 == hand[k]%13) {
            return true;
          }
        }
      }
    }
    return false;
  }
	public static boolean twoPair(int[] hand) {
		int numPair = 0;
		int hold = 14;
		for (int i=0; i<5; i++) {
      for (int j=0; j<i; j++) {
        if (hand[i]%13 == hand[j]%13) {
					if (hand[i]%13 != hold) {
          	numPair++;
						hold = hand[i]%13;
					}
        }
      }
    }
		if (numPair == 2) {
			return true;
		}
		else {
			return false;
		}
	}
  public static boolean pair(int[] hand) {
    for (int i=0; i<5; i++) {
      for (int j=0; j<i; j++) {
        if (hand[i]%13 == hand[j]%13) {
          return true;
        }
      }
    }
    return false;
  }
  public static int high(int[] hand) {
    int highest = 0;
    for (int i=0; i<5; i++) {
      if (hand[i]%13 > highest) {
        highest = hand[i]%13;
      }
    }
    return highest;
  }
  public static void shuffle(int[] deck){
		Random r = new Random();  // Random number generator			
		for (int i=0; i<deck.length; i++) {
		    int randomPosition = r.nextInt(deck.length);
		    int temp = deck[i];
		    deck[i] = deck[randomPosition];
		    deck[randomPosition] = temp;
    }
	}
  public static void print(int[] array) {
    for (int i=0; i<array.length; i++) {
      System.out.print("[" + array[i] + "]");
    }
    System.out.println();
  }
  public static int handCheck(int[] hand) {
    int level = 0;
    if (twoPair(hand) && threeOfK(hand)) {
      level = 5;
			System.out.println("Full House");
    }
		else if (flush(hand)) {
      level = 4;
			System.out.println("Flush");
    }
    else if (threeOfK(hand)) {
      level = 3;
			System.out.println("Three Of A Kind");
    }
		else if(twoPair(hand)){
			level = 2;
			System.out.println("Two Pair");
		}
    else if (pair(hand)) {
      level = 1;
			System.out.println("Pair");
    }
		else {
			System.out.println("High Card");
		}
    return level;
  }
}