// Ryan Kessler
// 4-17-18
// CSE 2
// HomeWork 09
import java.util.Random;
public class ShuffleTest {
  public static void main(String[] args) {
    int[] deck = new int[20]; // creating the array for the deck of cards
    for (int i=0; i<deck.length; i++) { // for loop to fill the deck
      deck[i] = i;
    }
    print(deck);
    deck = shuffle(deck);
    print(deck);
  }
  public static int[] shuffle(int[] deck){
		Random r = new Random();  // Random number generator			
		for (int i=0; i<deck.length; i++) {
		    int randomPosition = r.nextInt(deck.length);
		    int temp = deck[i];
		    deck[i] = deck[randomPosition];
		    deck[randomPosition] = temp;
    }
		return deck;
	}
  public static void print(int[] array) {
    for (int i=0; i<array.length; i++) {
      System.out.print("[" + array[i] + "]");
    }
    System.out.println();
  }
}

//for (int i=0; i<deck.length; i++) {
	//	    int randomPosition = r.nextInt(deck.length);
		//    int temp = deck[i];
		  //  deck[i] = deck[randomPosition];
		    //deck[randomPosition] = temp;
		//}