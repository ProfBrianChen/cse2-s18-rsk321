// Ryan Kessler
// CSE2
// 2-16-18
// Lab04
public class CardGenerator{
  // main method required for every Java program
  public static void main(String[] args) {
    int card = (int)(Math.random()*51)+1; // random number generator btwn 1 and 52
    String realCard; // defining the card assignment variable
    String suit = ""; // defining the suit assignment variable
    if ( 1 <= card && card <= 13 ) {
      suit = "Diamonds"; // determine if suit is diamonds
    } else if ( 14 <= card && card <= 26 ) {
      suit = "Clubs"; // determine if suit is clubs
      card = card - 13; // adjust card number for card assignment
    } else if ( 27 <= card && card <= 39 ) {
      suit = "Hearts"; // determine if suit is hearts
      card = card - 26; // adjust for card number assignment
    } else if ( 40 <= card && card <= 52 ) {
      suit = "Spades"; // determine if suit is spades
      card = card - 39; // adjust for card number assignment
    }
    if ( 2 <= card && card <= 10) {
      realCard = ""+card; // identify if the card is a numbered card
    } else if ( card == 11) {
      realCard = "Jack"; // identify if the card is a Jack
    } else if ( card == 12) {
      realCard = "Queen"; // identify if the card is a Queen
    } else if ( card == 13) {
      realCard = "King"; // identify if the card is a King
    } else {
      realCard = "Ace"; // identify if the card is an Ace
    }
    System.out.println("You picked the " + realCard + " of " + suit); // print the outcome
  }  //end of main method   
} //end of class