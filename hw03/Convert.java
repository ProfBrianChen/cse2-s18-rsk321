// Ryan Kessler
// CSE 2
// 2-13-18
// hw03 - Hurricane Conversion
import java.util.Scanner;
public class Convert {
  public static void main(String[] args) {
    // main method required for every Java program
    Scanner input = new Scanner( System.in );
    System.out.print("Enter the affected area in acres: "); // find out how large the affected area is in acres
    double areaAcre = input.nextDouble();
    System.out.print("Enter the rainfall in the affected area: ");  // find out the average rainfall in inches
    double rainInch = input.nextDouble();
    double sqMilesPerAcre = 0.0015624989;  // conversion factor for the amount of square miles in an acres
    double InchPerMile = 63360; // conversion factor for the amount of inches in a miles
    double areaMile = areaAcre * sqMilesPerAcre, // amount of miles covered by rain
    rainMile = rainInch / InchPerMile, // amount of rain in miles
    sqMilesRain = areaMile * rainMile; // calculation of the amount of rain in square miles 
    System.out.println(sqMilesRain + " cubic miles");
  } //end of main method   
} //end of class