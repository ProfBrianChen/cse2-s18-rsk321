// Ryan Kessler
// CSE 2
// 2-13-18
// hw03 - Pyramid Area Calculation
import java.util.Scanner;
public class Pyramid {
  public static void main(String[] args) {
    // main method required for every Java program
    Scanner input = new Scanner( System.in );
    System.out.print("The square side of the pyramid is (input length): "); // ask the user for the square side length
    double side = input.nextDouble();
    System.out.print("The height of the pyramid is (input height): "); // ask the user for the height
    double height = input.nextDouble();
    double volume = Math.pow(side,2) * height / 3; // calculate the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + volume);
  } //end of main method   
} //end of class
