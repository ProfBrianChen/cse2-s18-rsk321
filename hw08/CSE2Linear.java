// Ryan Kessler
// 4-10-18
// CSE 2
// HomeWork 08 - Array Searching
import java.util.Scanner;
public class CSE2Linear {
  public static void linear(int search, int grade[]) {
    int i = 0; // initialize the counting varaible
    boolean found = false; // we havn't found the value
    while (true) {
      if (search == grade[i]) {
        found = true; // We found the value!!
        break;
      }
      else {
        i++; // incrememnt i to search the next value in the array
        found = false;
      }
      if (i==grade.length) {
        break; // code to stop the search if we reached the end of the array
      }
    }
    int iteration = i+1;
    if (found == true) {
      System.out.println(search + " was found in the list with " + iteration + " iterations."); // print for if we found the grade
    }
    else {
      System.out.println(search + " was not found in the list with " + i + " iterations."); // if we didn't find the grade
    }
  }
  public static void binary(int search, int grade[]) {
    int searchStart = 0; // variable to start searching ta
    int searchEnd = grade.length; // end search at
    int iteration = 0; // how many times the loops has to run
    boolean found = false; // we have not yet found the value
    int middle = (int) (searchEnd+searchStart) / 2; // declaring the middle value (where we check for the wanted grade)
    while(grade[middle] !=  search) { // while loop to search for the grade
      if (grade[middle] < search) {
        searchStart = middle; // changing the start of the seach
      }
      else {
        searchEnd = middle; // changing the end of the search
      }
      middle = (int) (searchEnd+searchStart) / 2; // changing the middle value (where we check)
      iteration++; // increase the amount of times we've searched
      if (grade[middle]==search) {
        found = true; // We found the value!!
      }
      if (middle==searchStart || middle==searchEnd) { // if we have exhausted the search but have not found the grade
        break;
      }
     }
    if (found) {
      System.out.println(search + " was found with " + iteration + " iterations."); // print for if we found it
    }
    else if (!found) {
      System.out.println(search + " was not found with " + iteration + " iterations."); // print for if we didnt find it
    }
  }
  public static int[] scramble(int grade[]) {
    int[] gradeScramble = new int[grade.length];
    int[] holding = new int[grade.length];
    for (int j =0; j<grade.length; j++) {
      holding[j] = j; // creates the holding array
    }
    for (int i=0; i<grade.length; i++) {
      int r = (int)(Math.random()*(grade.length-i)); // get random number
      gradeScramble[i] = grade[holding[r]]; // takes the number from the array holding the remaining values
      holding[r] = grade.length-1-i;  // places last item at previous value
    }
    return gradeScramble; // returns the scrambled array
  }
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in); // import scanner
    int[] grades = new int[15]; // declare and allocate memory for grades
    for (int i=0; i<grades.length; i++) {
      if (i==0) {
        System.out.print("Please enter an integer between 0 and 100: "); // ask the user to input the first integer
      }
      else {
        System.out.print("Please enter an integer between 0 and 100 that is greater than the previous: "); // ask the user to input each following integer
      }
      while (true) { // while loop to make sure the user enters an allowed value
        while ( !input.hasNextInt() ) {
          System.out.print("Error - please enter an integer between 0 and 100: ");
          String junkWord = input.nextLine();
        }
        grades[i] = input.nextInt();
        while ( grades[i] < 0 || grades[i] > 100 ) {
          System.out.print("Error - the integer entered was not between 0 and 100: ");
          if ( input.hasNextInt() ) {
            grades[i] = input.nextInt();
          }
          else {
            break;
          }
        }
        if (i == 0) {
          if ( grades[i] >= 0 && grades[i] <= 100 ) {
            break;
          }
        }
        else {
          if ( grades[i] >= 0 && grades[i] <= 100 && grades[i]>=grades[i-1]) {
            break;
          }
          else {
            System.out.print("Error - please enter all integers in ascending order: ");
          }
        }
      }
    }
    for (int i=0; i<grades.length; i++) { // for loop to print the array
      System.out.print(grades[i] + " ");
    }
    System.out.println();
    int find1 = 0;
    System.out.println("Please enter a grade to search for between 0 and 100: "); // prompts the user to enter a grade to look for with binary search
    while (true) { // while loop to make sure the user enters a positve integer between 0 and 100
      while ( !input.hasNextInt() ) {
        System.out.print("Error - please enter an integer between 0 and 100: ");
        String junkWord = input.nextLine();
      }
      find1 = input.nextInt();
      while ( find1 < 0 || find1 > 100 ) {
        System.out.print("Error - please enter an integer between 0 and 100: ");
        if ( input.hasNextInt() ) {
          find1 = input.nextInt();
        }
        else {
          break;
        }
      }
      if ( find1 >= 0 && find1 <= 100 ) {
        break;
      }
    }
    binary(find1, grades); // calling the binary search method
    int[] gradesArray2 = new int[grades.length]; // declaring and allocating memory for the scrambled array
    gradesArray2 = scramble(grades); // calling the scramble array method
    for (int i=0; i<grades.length; i++) {
      System.out.print(gradesArray2[i] + " "); // printing the array
    }
    System.out.println();
    int find2 = 0;
    System.out.println("Please enter a grade to search for between 0 and 100: "); // asking the user for a grade to search for using linear search
    while (true) { // while loop to make sure the user enters an allowed value
      while ( !input.hasNextInt() ) {
        System.out.print("Error - please enter an integer between 0 and 100: ");
        String junkWord = input.nextLine();
      }
      find2 = input.nextInt();
      while ( find2 < 0 || find2 > 100 ) {
        System.out.print("Error - please enter an integer between 0 and 100: ");
        if ( input.hasNextInt() ) {
          find2 = input.nextInt();
        }
        else {
          break;
        }
      }
      if ( find2 >= 0 && find2 <= 100 ) {
        break;
      }
    }
    linear(find2, gradesArray2); // calling the linear search method
  }
}