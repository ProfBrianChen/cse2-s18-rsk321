import java.util.Scanner;
public class lab10{
  public static void main (String[] args) {
    Scanner input = new Scanner(System.in);
    System.out.print("Please enter an integer for the height of the array: ");
    int height = input.nextInt();
    System.out.print("Please enter an integer for the width of the array: ");
    int width = input.nextInt();
    System.out.print("Would you like the matrix to be generated in row-major representation? (y for yes): ");
    String junk = input.nextLine();
    String answer = input.nextLine();
    char check = answer.charAt(0);
    boolean format = false;
    if (check == 'y') {
      format = true;
    }
    int[][] matrix = increasingMatrix(height, width, format);
    printMatrix(matrix, format);
    if (!format) {
      int[][] array = translate(matrix);
      format = true;
      printMatrix(array, format);
      printMatrix(matrix, format);
    }
    
    
  }
  public static int[][] increasingMatrix(int height, int width, boolean format) {
    int[][] matrix;
    int count = 0;
    if (format) {
      matrix = new int[height][width];
      for (int i=0; i<height; i++) {
        for (int j=0; j<width; j++) {
          matrix[i][j] = count++;
        }
      }
    }
    else {
      matrix = new int[width][height];
      for (int i=0; i<height; i++) {
        for (int j=0; j<width; j++) {
          matrix[j][i] = count++;
        }
      }
    }
    return matrix;
  }
  public static int[][] translate(int[][] array) {
    int[][] rowArray = new int[array[0].length][array.length];
    for (int i=0; i<rowArray.length; i++) {
      for(int j=0; j<rowArray[0].length; j++) {
        rowArray[i][j] = array[j][i];
      }
    }
    return rowArray;
  }
  public static void printMatrix(int[][] array, boolean format) {
    if (format) {
      for (int i=0; i<array.length; i++) {
        for (int j=0; j<array[i].length; j++) {
          System.out.print(array[i][j] + " ");
        }
        System.out.println();
      }
    }
    else {
      for (int i=0; i<array[0].length; i++) {
        for (int j=0; j<array.length; j++) {
          System.out.print(array[j][i] + " ");
        }
        System.out.println();
      }
    }
  }
}