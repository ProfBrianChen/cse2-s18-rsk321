public class newlab10 {
  public static void main(String[] args) {
    int height1 = (int)(Math.random()*10)+1;
    int width1 = (int)(Math.random()*10)+1;
    boolean formatA = true;
    int[][] matrixA = increasingMatrix(height1, width1, formatA);
    System.out.println("Generating a matrix with height " + height1 + " and width " + width1 + ":");
    printMatrix(matrixA, formatA);
    boolean formatB = false;
    int[][] matrixB = increasingMatrix(height1, width1, formatB);
    System.out.println("Generating a matrix with height " + height1 + " and width " + width1 + ":");
    printMatrix(matrixB, formatB);
    
    int height2 = (int)(Math.random()*10)+1;
    int width2 = (int)(Math.random()*10)+1;
    boolean formatC = true;
    int[][] matrixC = increasingMatrix(height2, width2, formatC);
    System.out.println("Generating a matrix with height " + height2 + " and width " + width2 + ":");
    printMatrix(matrixC, formatC);
    
    int[][] AandB = addMatrix(matrixA, formatA, matrixB, formatB);
    int[][] AandC = addMatrix(matrixA, formatA, matrixC, formatC); 
  }
  public static int[][] increasingMatrix(int height, int width, boolean format) {
    int[][] matrix;
    int count = 0;
    if (format) {
      matrix = new int[height][width];
      for (int i=0; i<height; i++) {
        for (int j=0; j<width; j++) {
          matrix[i][j] = count++;
        }
      }
    }
    else {
      matrix = new int[width][height];
      for (int i=0; i<height; i++) {
        for (int j=0; j<width; j++) {
          matrix[j][i] = count++;
        }
      }
    }
    return matrix;
  }
  public static int[][] translate(int[][] array) {
    int[][] rowArray = new int[array[0].length][array.length];
    for (int i=0; i<rowArray.length; i++) {
      for(int j=0; j<rowArray[0].length; j++) {
        rowArray[i][j] = array[j][i];
      }
    }
    return rowArray;
  }
  public static int[][] addMatrix( int[][] a, boolean formatA, int[][] b, boolean formatB) {
    System.out.println("Attempting to add input matrices");
    int[][] rowA;
    int[][] rowB;
    if (!formatA) {
      rowA = translate(a);
      System.out.println("Translating column major to row major input");
    }
    else {
      rowA = a;
    }
    printMatrix(a, formatA);
    System.out.println("plus");
    if (!formatB) {
      rowB = translate(b);
      System.out.println("Translating column major to row major input");
    }
    else {
      rowB = b;
    }
    printMatrix(b, formatB);
    int[][] added = new int[rowA.length][rowA[0].length];
    if (rowA.length == rowB.length && rowA[0].length == rowB[0].length) {
      for (int i=0; i<added.length; i++) {
        for (int j=0; j<added[0].length; j++) {
          added[i][j] = rowA[i][j] + rowB[i][j];
        }
      }
      System.out.println("Added input matrices");
      printMatrix(added, true);
      return added;
    }
    else {
      System.out.println("Unable to add input matrices");
      return null;
    }
  }
  public static void printMatrix(int[][] array, boolean format) {
    if (format) {
      for (int i=0; i<array.length; i++) {
        for (int j=0; j<array[i].length; j++) {
          System.out.printf("%4d",array[i][j]);
        }
        System.out.println();
      }
    }
    else {
      for (int i=0; i<array[0].length; i++) {
        for (int j=0; j<array.length; j++) {
          System.out.printf("%4d",array[j][i]);
        }
        System.out.println();
      }
    }
  }
}