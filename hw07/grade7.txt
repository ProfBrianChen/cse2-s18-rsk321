=============================
grade7.txt
=============================
Grade: 97/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
The code compiles

B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
The code runs properly

C) How can any runtime errors be resolved?
N/A

D) What topics should the student study in order to avoid the errors they made in this homework?
N/A

E) Other comments:
Great job! I took off a few points because not all error input was accounted for (ex. negative numbers, ints instead of doubles)