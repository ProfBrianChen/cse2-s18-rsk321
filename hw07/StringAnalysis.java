// Ryan Kessler
// 3-27-18
// CSE 2
// HW 07 - String Analysis
import java.util.Scanner;
public class StringAnalysis {
  public static void charCheck(String checkString, int checkLength) { // method with a user inputted test length
    int a = 0;
    if (checkLength > checkString.length()) { // checking to see if the integer is longer than the inputted string
      a = checkString.length();
    }
    else {
      a = checkLength;
    }
    for(int i=0; i < a; i++) { // for loop to check each character
      char charCheck = checkString.charAt(i);
      if ('a' <= charCheck && 'z' >= charCheck) { // if the character is between a and z it is a letter
        System.out.println(charCheck + " is a letter.");
      }
      else {
        System.out.println(charCheck + " is not a letter.");
      }
    }
  }
  
  public static void charCheck(String checkString) { // method to check the entire string
    for(int i=0; i < checkString.length(); i++) { // for loop to check each character
      char charCheck = checkString.charAt(i);
      if ('a' <= charCheck && 'z' >= charCheck) { // if the character is between a and z it is a letter
        System.out.println(charCheck + " is a letter.");
      }
      else {
        System.out.println(charCheck + " is not a letter.");
      }
    }
  }
  
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in); // creating a scanner
    System.out.print("Enter a String: "); // asking the user to input a string
    String testString = input.next();
    System.out.print("If you would like to test a certain number of characters enter an integer, otherwise enter anything to proceed: "); // asking the user if they would like to define the test length
    if (input.hasNextInt()) { // deciding which method to use
      int testInt = input.nextInt();
      charCheck(testString, testInt);
    }
    else {
      charCheck(testString);
    }
  }
}