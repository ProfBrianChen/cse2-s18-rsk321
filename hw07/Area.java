// Ryan Kessler
// 3-27-18
// CSE 2
// HW 07 - Area
import java.util.Scanner;
public class Area {
  
  public static String check() {
    Scanner input = new Scanner( System.in ); // creating the scanner
    System.out.print("Please enter the shape you would like to find the area of - circle, triangle, or rectangle: "); // asking the user what shape they would like to find the area of
    String shape = input.nextLine();
    while (true) { // while loop to make sure the user inputted an acceptable shape
      if (shape.equals("circle")) {
        break;
      }
      else if (shape.equals("rectangle")) {
        break;
      }
      else if (shape.equals("triangle")) {
        break;
      }
      else {
      System.out.print("Error - please enter circle, triangle, or rectangle: ");
      shape = input.nextLine();
      }
    }
    return shape; // return which shape the user would like to use
  }
  
  public static double rectangle() { // method to calculate the area of a rectangle
    Scanner input = new Scanner( System.in );
    
    System.out.print("Enter the length of the base of the rectangle: "); // asks user for the base
    while (!input.hasNextDouble()) { // loop to make sure user entered correctly
      System.out.print("Error - please enter the length of the base as a double: ");
      String junkWord = input.nextLine();
    }
    double length = input.nextDouble();
    
    System.out.print("Enter the height of the rectangle: "); // asks user for height
    while (!input.hasNextDouble()) { // loop to make sure user entered correctly
      System.out.print("Error - please enter the height of the rectangle as a double: ");
      String junkWord = input.nextLine();
    }
    double height = input.nextDouble();
    
    double area = length * height; // calculate area
    return area; // returns the area to be used in the main method
  }
  
  public static double triangle() { // method to calculate the area of a triangle
    Scanner input = new Scanner( System.in );
    
    System.out.print("Enter the length of the base of the triangle: "); // asks user for the base
    while (!input.hasNextDouble()) { // loop to make sure user entered correctly
      System.out.print("Error - please enter the length of the base as a double: ");
      String junkWord = input.nextLine();
    }
    double length = input.nextDouble();
    
    System.out.print("Enter the height of the triangle: "); // asks user for the height
    while (!input.hasNextDouble()) { // loop to make sure user entered correctly
      System.out.print("Error - please enter the height of the triangle as a double: ");
      String junkWord = input.nextLine();
    }
    double height = input.nextDouble();
    
    double area = length * height / 2; // calculate area
    return area; // returns the area to be used in the main method
  }
  
  public static double circle() { // method to calculate the area of a circle
    Scanner input = new Scanner( System.in );
    
    System.out.print("Enter the radius of the circle: "); // asks user for the radius
    while (!input.hasNextDouble()) { // loop to make sure user entered correctly
      System.out.print("Error - please enter the length of the base as a double: ");
      String junkWord = input.nextLine();
    }
    double radius = input.nextDouble();
    
    double area = Math.pow(radius, 2) * Math.PI; // calculate area
    return area;  // returns the area to be used in the main method
  }
  
  public static void main(String[] args) {
    String shape = check();
    double area = 0;
    if (shape.equals("circle")) { // statements to determine which shape the area was taken of and to print accordingly
        area = circle();
      }
      else if (shape.equals("rectangle")) {
        area = rectangle();
      }
      else if (shape.equals("triangle")) {
        area = triangle();
      }
    System.out.println("The area of the " + shape + " is " + area);
  }
  
}