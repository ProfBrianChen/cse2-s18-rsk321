public class lab09 {
  public static void main(String[] args) {
    int[] array0 = new int[]{1,2,3,4,5,6,7,8,9,10};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    print(array0);
    print(array1);
    print(array2);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    print(array3);
  }
  public static int[] copy(int[] array) {
    int[] arrayCopy = new int[array.length];
    for(int i=0; i<array.length; i++) {
      arrayCopy[i] = array[i];
    }
    return arrayCopy;
  }
  public static void inverter(int[] array) {
    for(int i=0; i<array.length/2; i++) {
      int first = array[i];
      int last = array[array.length-i-1];
      array[i] = last;
      array[array.length-i-1] = first;
    }
  }
  public static int[] inverter2(int[] array) {
    int[] arrayInvert2 = copy(array);
    for(int i=0; i<array.length; i++) {
      arrayInvert2[i] = array[(array.length)-i-1];
    }
    return arrayInvert2;
  }
  public static void print(int[] array) {
    for (int i=0; i<array.length; i++) {
      System.out.print("[" + array[i] + "] ");
    }
    System.out.println();
  }
}