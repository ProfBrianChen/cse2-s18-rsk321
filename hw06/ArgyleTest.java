// Ryan Kessler
// 3 - 20 - 18
// CSE 2
// HomeWork 06
import java.util.Scanner;
public class ArgyleTest {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    // width of Viewing window
    System.out.print("please enter a positive integer for the width of Viewing window in characters: ");
    String windowWidthString = myScanner.nextLine();
    char windowWidthResult = windowWidthString.charAt(0);
    int windowWidth = (int) windowWidthResult;
    
    // height of Viewing window
    System.out.print("please enter a positive integer for the height of Viewing window in characters: ");
    String windowHeightString = myScanner.nextLine();
    char windowHeightResult = windowHeightString.charAt(0);
    int windowHeight = (int) windowHeightResult;
    
    // width of the argyle diamonds
    System.out.print("please enter a positive integer for the width of the argyle diamond: ");
    int argyleWidth = 0;
    while (true) {
      while ( !myScanner.hasNextInt() ) {
        System.out.print("Error - please enter a positive integer: ");
        String junkWord = myScanner.nextLine();
      }
      argyleWidth = myScanner.nextInt();
      while ( argyleWidth < 1) {
        System.out.print("Error - please enter a positive integer: ");
        if ( myScanner.hasNextInt() ) {
          argyleWidth = myScanner.nextInt();
        }
        else {
          break;
        }
      }
      if ( argyleWidth >= 1) {
        break;
      }
    }
    
    // width of the argyle center stripe
    System.out.print("please enter a positive odd integer for the width of the argyle diamond: ");
    int argyleCenter = 0;
    while (true) {
      while ( !myScanner.hasNextInt() ) {
        System.out.print("Error - please enter a positve odd integer: ");
        String junkWord = myScanner.nextLine();
      }
      argyleCenter = myScanner.nextInt();
      while ( argyleCenter < 1 || (argyleCenter % 2 == 0) ) {
        System.out.print("Error - please enter a positive odd integer: ");
        if ( myScanner.hasNextInt() ) {
          argyleCenter = myScanner.nextInt();
        }
        else {
          break;
        }
      }
      if ( argyleCenter >= 1) {
        break;
      }
    }
    
    // 3 filling characters
    String junk = myScanner.nextLine();
    System.out.print("please enter a first character for the pattern fill: ");
    String firstString = myScanner.next();
    char firstChar = firstString.charAt(0);
    System.out.print("please enter a second character for the pattern fill: ");
    String secondString = myScanner.next();
    char secondChar = secondString.charAt(0);
    System.out.print("please enter a third character for the stripe fill: ");
    String thirdString = myScanner.next();
    char thirdChar = thirdString.charAt(0);
    
    System.out.println(windowWidth + " " + windowHeight + " " + argyleWidth + " " + argyleCenter + " " + firstChar + " " + secondChar + " " + thirdChar);
    
    for (int i=0; i<argyleWidth/2; i++) {
      for(int j=0; j<=argyleWidth/2; j++) {
        if( j==i ) {
          for(int s=0; s<argyleCenter; s++) {
            System.out.print(thirdChar);
            j++;
            if(j==argyleWidth/2) {
              break;
            }
          }
        }
        else {
          if (j <= argyleWidth/2 - 1 - i) {
            // System.out.print(firstChar);
          }
          else {
            System.out.print(secondChar);
          }
        }
        if(j==argyleWidth/2) {
          break;
        }
      }
      System.out.println();
    }
    
    System.out.println();
    
    for (int i=0; i<argyleWidth/2; i++) {
      for(int j=0; j<=argyleWidth/2; j++) {
        if( j==argyleWidth/2-i-argyleCenter ) {
          for(int s=0; s<argyleCenter; s++) {
            System.out.print(thirdChar);
            j++;
            if(j==argyleWidth/2) {
              break;
            }
          }
        }
        else {
          if (j < i) {
            System.out.print(secondChar);
          }
          else {
            System.out.print(firstChar);
          }
        }
        if(j==argyleWidth/2) {
          break;
        }
      }
      System.out.println();
    }
    
    System.out.println();
    
    for (int i=0; i<argyleWidth/2; i++) {
      for(int j=0; j<=argyleWidth/2; j++) {
        if( j==argyleWidth/2-i-argyleCenter ) {
          for(int s=0; s<argyleCenter; s++) {
            System.out.print(thirdChar);
            j++;
            if(j==argyleWidth/2) {
              break;
            }
          }
        }
        else {
          if (j < i) {
            System.out.print(firstChar);
          }
          else {
            System.out.print(secondChar);
          }
        }
        if(j==argyleWidth/2) {
          break;
        }
      }
      System.out.println();
    }
    
    System.out.println();
    
    for (int i=0; i<argyleWidth/2; i++) {
      for(int j=0; j<=argyleWidth/2; j++) {
        if( j==i ) {
          for(int s=0; s<argyleCenter; s++) {
            System.out.print(thirdChar);
            j++;
            if(j==argyleWidth/2) {
              break;
            }
          }
        }
        else {
          if (j <= argyleWidth/2 - 1 - i) {
            System.out.print(secondChar);
          }
          else {
            System.out.print(firstChar);
          }
        }
        if(j==argyleWidth/2) {
          break;
        }
      }
      System.out.println();
    }
    
  }
}