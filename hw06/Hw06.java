// Ryan Kessler
// 3 - 20 - 18
// CSE 2
// HomeWork 06
import java.util.Scanner;
public class Hw06 {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    // width of Viewing window
    System.out.print("please enter a positive integer for the width of Viewing window in characters: ");
    String windowWidthString = myScanner.nextLine();
    char windowWidthResult = windowWidthString.charAt(0);
    int windowWidth = (int) windowWidthResult;
    
    // height of Viewing window
    System.out.print("please enter a positive integer for the height of Viewing window in characters: ");
    String windowHeightString = myScanner.nextLine();
    char windowHeightResult = windowHeightString.charAt(0);
    int windowHeight = (int) windowHeightResult;
    
    // width of the argyle diamonds
    System.out.print("please enter a positive integer for the width of the argyle diamond: ");
    int argyleWidth = 0;
    while (true) {
      while ( !myScanner.hasNextInt() ) {
        System.out.print("Error - please enter a positive integer: ");
        String junkWord = myScanner.nextLine();
      }
      argyleWidth = myScanner.nextInt();
      while ( argyleWidth < 1) {
        System.out.print("Error - please enter a positive integer: ");
        if ( myScanner.hasNextInt() ) {
          argyleWidth = myScanner.nextInt();
        }
        else {
          break;
        }
      }
      if ( argyleWidth >= 1) {
        break;
      }
    }
    
    // width of the argyle center stripe
    System.out.print("please enter a positive odd integer for the width of the argyle center stripe: ");
    int argyleCenter = 0;
    while (true) {
      while ( !myScanner.hasNextInt() ) {
        System.out.print("Error - please enter a positve odd integer: ");
        String junkWord = myScanner.nextLine();
      }
      argyleCenter = myScanner.nextInt();
      while ( argyleCenter < 1 || (argyleCenter % 2 == 0) ) {
        System.out.print("Error - please enter a positive odd integer: ");
        if ( myScanner.hasNextInt() ) {
          argyleCenter = myScanner.nextInt();
        }
        else {
          break;
        }
      }
      if ( argyleCenter >= 1) {
        break;
      }
    }
    
    // 3 filling characters
    String junk = myScanner.nextLine();
    System.out.print("please enter a first character for the pattern fill: ");
    String firstString = myScanner.next();
    char firstChar = firstString.charAt(0);
    System.out.print("please enter a second character for the pattern fill: ");
    String secondString = myScanner.next();
    char secondChar = secondString.charAt(0);
    System.out.print("please enter a third character for the stripe fill: ");
    String thirdString = myScanner.next();
    char thirdChar = thirdString.charAt(0);
        
    char array [][] = new char[windowHeight*windowHeight][windowWidth*windowWidth]; 
   
    // covers the array in the first character
    for(int i=0; i<windowHeight; i++) {
      for(int j=0; j<windowWidth; j++) {
        array [i][j] = firstChar;
      }
    }
    
    // inputs the first half of the diamonds with the second character
    int alter1 = 0;
    int a =0;
    while (a<=windowWidth/argyleWidth) {
      while (alter1 < windowWidth) {
        for (int i=0; i<windowHeight; i++) {
          for(int j=0; j<windowWidth; j++) {
            if (j >= argyleWidth/2 - 1 - i + alter1) {
              array [a*argyleWidth+i][j] = secondChar;
              array [a*argyleWidth+argyleWidth-i-1][j] = secondChar;
            }
            if(j==argyleWidth/2 + alter1 || i==argyleWidth/2) {
              break;
            }
          }
          if(i == argyleWidth/2) {
            break;
          }
        }
        alter1 += argyleWidth;
      }
      a++;
      alter1 = 0;
    }
    
    // inputs the second half of the diamonds with the second character
    int alter2 = 0;
    int b = 0;
    while (b<=windowWidth/argyleWidth) {
      while (alter2 < windowWidth) {
        for (int i=0; i<windowHeight; i++) {
          for(int j=0; j<windowWidth; j++) {
            if (j >= argyleWidth/2 + alter2 && j <= argyleWidth/2 - 1 + i + alter2) {
              array [b*argyleWidth+i][j] = secondChar;
              array [b*argyleWidth+argyleWidth-i-1][j] = secondChar;
            }
            if(i == argyleWidth/2) {
              break;
            }
          }
          if(i == argyleWidth/2) {
            break;
          }
        }
        alter2 += argyleWidth;
      }
      b++;
      alter2 = 0;
    }
    
    // writes the backslash part of the argyle stripe
    int alterW = 0;
    while (alterW < windowWidth) {
      for(int i=0; i<windowHeight; i++) {
        for(int j=0; j<windowWidth; j++) {
          if (i == j - alterW) {
            for(int s=0; s<argyleCenter; s++) {
              array[i][j] = thirdChar;
              j++;
            }
          }
        }
      }
      for(int i=0; i<windowHeight; i++) {
        for(int j=0; j<windowWidth; j++) {
          if ( j == argyleWidth - i - 1 + alterW) {
            for(int s=0; s<argyleCenter; s++) {
              array[i][j] = thirdChar;
              j++;
            }
          }
        }
      }
      alterW += argyleWidth;
    }
    
    // writes the forward slash half of the argyle stripe
    int alterH = 0;
    while (alterH < windowHeight) {
      for(int i=0; i<windowHeight; i++) {
        for(int j=0; j<windowWidth; j++) {
          if (j == i - alterH) {
            for(int s=0; s<argyleCenter; s++) {
              array[i][j] = thirdChar;
              j++;
            }
          }
        }
      }
      for(int i=0; i<windowHeight; i++) {
        for(int j=0; j<windowWidth; j++) {
          if ( j == argyleWidth - i - 1 + alterH) {
            for(int s=0; s<argyleCenter; s++) {
              array[i][j] = thirdChar;
              j++;
            }
          }
        }
      }
      alterH += argyleWidth;
    }
    
    // prints out the argyle pattern
    for(int i=0; i<windowHeight; i++) {
      for(int j=0; j<windowWidth; j++) {
        System.out.print(array [i][j]);
      }
      System.out.println();
    }
    
  }
}