// Ryan Kessler
// 4 - 24 - 18
// CSE 2
// HomeWork 10 - Robot City
public class RobotCity {
  public static void main(String[] args) {
    int[][] cityArray = buildCity();
    display(cityArray);
    int k = (int)(Math.random()*100);
    System.out.println(k);
    invade(cityArray, k);
    display(cityArray);
    for (int i=0; i<5; i++) {
      System.out.println();
      update(cityArray, i);
      display(cityArray);
    }
  }
  public static int[][] buildCity() {
    int[][] cityArray = new int[(int)(Math.random()*6)+10][(int)(Math.random()*6)+10];
    for (int i=0; i<cityArray.length; i++) {
      for (int j=0; j<cityArray[0].length; j++) {
        cityArray[i][j] = (int)(Math.random()*900)+100;
      }
    }
    return cityArray;
  }
  public static void invade(int[][] cityArray, int k) {
    int totalElements = cityArray.length *cityArray[0].length;
    int[] holding = new int[totalElements];
    for(int i=0; i<totalElements; i++) {
      holding[i] = i;
    }
    int x, y;
    for (int i=0; i<k; i++) {
      do {int r = (int)(Math.random()*(totalElements-i));
        int loc = holding[r];
        holding[r] = totalElements-i;
        x = loc%cityArray.length;
        y = (int) (loc/cityArray.length);
      } while (x >= cityArray.length || y >= cityArray[0].length);
      int temp = cityArray[x][y];
      cityArray[x][y] = -temp;
    }
  }
  public static void update(int[][] cityArray, int num) {
    for (int i=num; i<cityArray.length; i++) {
      for (int j=0; j<cityArray[0].length; j++) {
        if (i<cityArray.length-1 && cityArray[i][j]<0 && cityArray[i+1][j]>0) {
          if (i-1>num && cityArray[i-num-1][j]>0 ) {
            int temp = cityArray[i+1][j];
            cityArray[i+1][j] = -temp;
          }
          else if (i-num==0 && cityArray[i][j]<0 && cityArray[i+1][j]>0) {
            int temp = cityArray[i+1][j];
            cityArray[i+1][j] = -temp;
          }
        }
      }
    }
  }
  public static void display(int[][] array) {
    for (int j=array[0].length-1;j>=0;j--) {
      for (int i=0; i<array.length; i++) {
        System.out.printf("%6d",array[i][j]);
      }
      System.out.println();
    }
  }
}