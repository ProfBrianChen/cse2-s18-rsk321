// Ryan Kessler
// CSE 2
// 2-11-18
// hw02 - Arithmetic Calculations
public class Arithmetic {
  public static void main(String[] args) {
    int numPants = 3; // Number of pairs of pants
    double pantsPrice = 34.98; // Cost per pair of pants
    int numShirts = 2; // Number of shirts
    double shirtsPrice = 24.99; // Cost per shirt
    int numBelts = 1; // Number of belts
    double beltPrice = 33.99; // Cost per belt
    double paSalesTax = 0.06; // The tax rate
    double totCostPants, totCostShirts, totCostBelts, totCost, // total costs for each item
    pantsTax, shirtTax, beltTax, totTax, // sales tax on each item
    totTransaction; // total cost of the transaction including tax
    // Calculations Below
    totCostPants = numPants * pantsPrice; // cost of all pants
    pantsTax = totCostPants * paSalesTax; // total sales tax on pants
    totCostShirts = numShirts * shirtsPrice; // cost of all shirts
    shirtTax = totCostShirts * paSalesTax; // total sales tax on shirts
    totCostBelts = numBelts * beltPrice; // cost of all belts
    beltTax = totCostBelts * paSalesTax; // total sales tax on belts
    totCost = totCostPants + totCostShirts + totCostBelts; // total cost before tax
    totTax = pantsTax + shirtTax + beltTax; // total tax paid
    totTransaction = totCost + totTax; // total of the entire transaction, including tax
    // Begin printing to display
    System.out.println("Buying "+numPants+" pairs of pants would cost $"+totCostPants);
    System.out.println("Buying "+numShirts+" shirts would cost $"+totCostShirts);
    System.out.println("Buying "+numBelts+" belt would cost $"+totCostBelts);
    System.out.println("The total price of the items is $"+totCost);
    System.out.printf("The total sales tax is $%.2f\n\n",totTax);
    System.out.printf("The total cost of the transaction is $%.2f\n",totTransaction);
  }
}