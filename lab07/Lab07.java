// Ryan Kessler
// 3-25-18
// CSE 2
// Lab 07
import java.util.Random;
public class Lab07 {
  
  public static String adjective() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String adj = "";
    switch (randomInt) {
      case 0:
        adj = "chatty";
        break;
      case 1:
        adj = "gloomy";
        break;
      case 2:
        adj = "quick";
        break;
      case 3:
        adj = "slow";
        break;
      case 4:
        adj = "scary";
        break;
      case 5:
        adj = "dehydrated";
        break;
      case 6:
        adj = "great";
        break;
      case 7:
        adj = "ugly";
        break;
      case 8:
        adj = "stupid";
        break;
      case 9:
        adj = "annoying";
        break;
    }
    return adj;
  }
  
  public static String subject() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String sub = "";
    switch (randomInt) {
      case 0:
        sub = "girl";
        break;
      case 1:
        sub = "fox";
        break;
      case 2:
        sub = "dog";
        break;
      case 3:
        sub = "student";
        break;
      case 4:
        sub = "cow";
        break;
      case 5:
        sub = "businessman";
        break;
      case 6:
        sub = "athlete";
        break;
      case 7:
        sub = "nerd";
        break;
      case 8:
        sub = "celebrity";
        break;
      case 9:
        sub = "teacher";
        break;
    }
    return sub;
  }
  
  public static String pastVerb() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String verb = "";
    switch (randomInt) {
      case 0:
        verb = "passed";
        break;
      case 1:
        verb = "ran with";
        break;
      case 2:
        verb = "ate with";
        break;
      case 3:
        verb = "created";
        break;
      case 4:
        verb = "hugged";
        break;
      case 5:
        verb = "played with";
        break;
      case 6:
        verb = "saw";
        break;
      case 7:
        verb = "hid from";
        break;
      case 8:
        verb = "doesn't like";
        break;
      case 9:
        verb = "sat on";
        break;
    }
    return verb;
  }
  
  public static String object() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String obj = "";
    switch (randomInt) {
      case 0:
        obj = "kid";
        break;
      case 1:
        obj = "parent";
        break;
      case 2:
        obj = "friend";
        break;
      case 3:
        obj = "turtle";
        break;
      case 4:
        obj = "monkey";
        break;
      case 5:
        obj = "lion";
        break;
      case 6:
        obj = "seal";
        break;
      case 7:
        obj = "zookeeper";
        break;
      case 8:
        obj = "president";
        break;
      case 9:
        obj = "worker";
        break;
    }
    return obj;
  }
  
  public static String thesis() {
    String sentenceAdj1 = adjective();
    String sentenceSub = subject();
    String sentenceVerb = pastVerb();
    String sentenceAdj2 = adjective();
    String sentenceObj = object();
    System.out.println("The " + sentenceAdj1 + " "+ sentenceSub + " " + sentenceVerb + " the " + sentenceAdj2 + " " + sentenceObj + ".");
    return sentenceSub;
  }
  
  public static String action() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    int randInt = randomGenerator.nextInt(10);
    String adj2 = "";
    switch (randomInt) {
      case 0:
        adj2 = "wierdly";
        break;
      case 1:
        adj2 = "particularly";
        break;
      case 2:
        adj2 = "unusually";
        break;
      case 3:
        adj2 = "nervously";
        break;
      case 4:
        adj2 = "brutally";
        break;
      case 5:
        adj2 = "playfully";
        break;
      case 6:
        adj2 = "bluntly";
        break;
      case 7:
        adj2 = "quite";
        break;
      case 8:
        adj2 = "pretty";
        break;
      case 9:
        adj2 = "childishly";
        break;
    }
    String act = "";
    switch (randInt) {
      case 0:
        act = "mean";
        break;
      case 1:
        act = "nice";
        break;
      case 2:
        act = "friendly";
        break;
      case 3:
        act = "kind";
        break;
      case 4:
        act = "hurtful";
        break;
      case 5:
        act = "playful";
        break;
      case 6:
        act = "comical";
        break;
      case 7:
        act = "sincere";
        break;
      case 8:
        act = "honest";
        break;
      case 9:
        act = "sweet";
        break;
    }
    String acting = adj2 + " " + act;
    return acting;
  }
  
  public static void main(String[] args) {
    Random randomGenerator = new Random();
    int actionInt = randomGenerator.nextInt(5);
    String subject = thesis();
    switch (actionInt) {
      case 4:
        String action4 = action();
        String actionAdj4 = adjective();
        String actionObj4 = object();
        System.out.println("This " + subject + " was " + action4 + " to the " + actionAdj4 + " " + actionObj4 + ".");
      case 3:
        String action3 = action();
        String actionAdj3 = adjective();
        String actionObj3 = object();
        System.out.println("This " + subject + " was " + action3 + " to the " + actionAdj3 + " " + actionObj3 + ".");
      case 2:
        String action2 = action();
        String actionAdj2 = adjective();
        String actionObj2 = object();
        System.out.println("This " + subject + " was " + action2 + " to the " + actionAdj2 + " " + actionObj2 + ".");
      case 1:
        String action1 = action();
        String actionAdj1 = adjective();
        String actionObj1 = object();
        System.out.println("This " + subject + " was " + action1 + " to the " + actionAdj1 + " " + actionObj1 + ".");
      case 0:
        String action0 = action();
        String actionAdj0 = adjective();
        String actionObj0 = object();
        System.out.println("This " + subject + " was " + action0 + " to the " + actionAdj0 + " " + actionObj0 + ".");
        break;
    }
    String conclusionVerb = pastVerb();
    String conclusionAdj = adjective();
    String conclusionObj = object();
    System.out.println("That " + subject + " " + conclusionVerb + " their " + conclusionAdj + " " + conclusionObj + "!");
  }
}