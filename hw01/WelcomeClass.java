// Ryan Kessler
// CSE2
// 1-30-18
// hw01
public class WelcomeClass {
  public static void main(String[] args) {
    // prints the welcome message
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-R--S--K--3--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    // prints the short autobiographic statement
    System.out.println("I am an avid New York sports fan, specifically the Mets, Nets, Jets and Devils");
  }
}