// Ryan Kessler
// CSE2
// 3-9-18
// Lab06
import java.util.Scanner; // import scanner
public class encrypted_x {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Please enter an integer between 1 and 100: ");
    int input = 0;
    while (true) {
      while ( !myScanner.hasNextInt() ) {
        System.out.print("Error - please enter an integer between 1 and 100: ");
        String junkWord = myScanner.nextLine();
      }
      input = myScanner.nextInt();
      while ( input < 1 || input > 100 ) {
        System.out.print("Error - please enter an integer between 1 and 100: ");
        if ( myScanner.hasNextInt() ) {
          input = myScanner.nextInt();
        }
        else {
          break;
        }
      }
      if ( input >= 1 && input <= 100 ) {
        break;
      }
    }
    System.out.println(input);
    for(int i = 0; i < input; i++) {
      for(int j = 0; j < input; j++) {
        if ( j == i || j == input - i - 1 ) {
          System.out.print(" ");
        }
        else {
          System.out.print("*");
        }
      }
      System.out.println();
    }
  }
}